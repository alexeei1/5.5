variable project_id {}

terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "google" {
  project = var.project_id
  region = "europe-north1"
  zone = "europe-north1-a"
}


