variable private_key_path {
}

variable user {
default = "yanaleshenko3108"
}

variable pub_key_path {
}

variable "countvm" {
  default = "2"
}
variable "indx" {
 default = "0"
}



data "google_compute_image" "debianimage" {
family = "debian-10"
project = "debian-cloud"
}


resource "google_compute_instance"  "instance"  {
  count = "${var.countvm}"
  name = "node${count.index}"
  machine_type = "e2-small"
  
  connection {
   user = "${var.user}"
   private_key = "${file(var.private_key_path)}"
   timeout = "2m"
   host = "${self.network_interface.0.access_config.0.nat_ip}"
  }
  provisioner "remote-exec" {
    inline = [
    "sudo apt update"
    ]   
  }
  
  provisioner "local-exec"  {
    command = "ansible-playbook -i '${self.network_interface.0.access_config.0.nat_ip},' -u '${var.user}'  --private-key '${var.private_key_path}' less38.yml"
  }

  boot_disk {
    initialize_params  {
     image = "${data.google_compute_image.debianimage.self_link}"
     }
    }
  network_interface {
     network = "default"
     access_config {
       
    }
   }
  lifecycle {
    ignore_changes = [attached_disk]
  }
 
}


